// Mobile Safari hide address bar
window.addEventListener("load",function() {
  setTimeout(function(){
    window.scrollTo(0, 1);
  }, 0);
});
$(document).ready(function(){	
	// Tip calculator
	function calculateTip() {
		// Handling subtotal box
		subtotal = parseFloat($('#subtotal').val());
		total = subtotal;
		// Handling tip and tax boxes
		$('input[data-percent]').each(function(){

			var percentRaw = parseInt($(this).val());
			if (isNaN(percentRaw)) {
				percentRaw = 0;
			}
			var percentage = (percentRaw / 100);

			var markup = subtotal * percentage;

			if(isNaN(markup)) {
				markup = 0;
			}
			roundedMarkup = markup.toFixed(2);

			$('.amounts span[class=' + $(this).attr('id') + ']').text('$' + roundedMarkup);

			total += markup;
			if(isNaN(total)) {
				total = 0;
			}

			roundedTotal = total.toFixed(2);
		});
		// Show results
		$('.result span').text('$' + roundedTotal);
	}
	// Call function whenever values change
	$('input[type=number]').keyup(calculateTip);
	calculateTip();
});