#Tip Calculator

![Screenshot of the Tip Calculator](https://bitbucket.org/tinacious/tip-calculator/raw/f1ddd7452dab1fde1934efd16f1dca4d3137fdf7/ref/screen1-sm.png) ![Photo of the Tip Calculator on a white iPhone 4s](http://onlycode.ca/tip/feature-img.jpg)

This is a jQuery-powered tip calculator that calculates the tip and tax on the subtotal as opposed to a total amount.

An iOS-optimized icon is available so that users can add the tip calculator to their home page.

##Use It
You can use the tip calculator here: [onlycode.ca/tip](http://onlycode.ca/tip)

##Developer Info
###Tina Holly
Feel free to contact me by [email](mailto:tinacious.design@gmail.com) or through one of my websites ([tinaciousdesign.com](http://tinaciousdesign.com) or [onlycode.ca](http://onlycode.ca)).
